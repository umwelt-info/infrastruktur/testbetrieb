#!/bin/bash
mkdir -p kc-data
chown 1000 kc-data

echo -e "\nMEM_TOTAL=$(awk '/MemTotal/{print $2}' /proc/meminfo)" >>.env

docker compose pull

docker compose up --detach

docker exec sfs sh -c "yarn dist:schema"
