#!/bin/bash
set +x
source .env
set -x

docker compose pause keycloak

cp kc-data/keycloakdb.mv.db /mnt/datacube/keycloakdb.mv.db.bak

docker compose unpause keycloak

mv /mnt/datacube/mariadb-latest /mnt/datacube/mariadb-older || true
docker exec mariadb mariabackup --backup --target-dir=/backup/mariadb-latest --user=${MARIADB_ROOT_USER} --password=${MARIADB_ROOT_PASSWORD}
rm -r /mnt/datacube/mariadb-older || true

docker exec mongo mongodump -o /backup

mkdir -p /mnt/datacube/solr
chown -R 8983 /mnt/datacube/solr

rm -r /mnt/datacube/solr/snapshot.older || true
mv /mnt/datacube/solr/snapshot.latest /mnt/datacube/solr/snapshot.older || true
curl "http://localhost:8983/solr/dc_shard1_replica_n1/replication?command=backup&location=/backup&name=latest"

sync
