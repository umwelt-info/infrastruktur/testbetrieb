#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.request import urlopen
from urllib.error import URLError
import json


def fetch_status(service):
    try:
        with urlopen(f"https://dc.tb.umwelt.info/{service}/health") as response:
            response = json.load(response)

        status = int(response["service"]["status"] == "Healthy")
    except URLError:
        status = 0

    return status


def collect_metrics():
    lines = [
        "# HELP datacube_health Indicates health of various Data Cube services.",
        "# TYPE datacube_heatlh gauge",
    ]

    for service in ["auth", "transfer", "design", "release"]:
        status = fetch_status(service)

        lines.append(f'datacube_health{{service="{service}"}} {status}')

    return "\n".join(lines)


class PrometheusProxyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        metrics = collect_metrics()

        self.send_response(200)
        self.send_header("content-type", "text/plain")
        self.end_headers()

        self.wfile.write(metrics.encode("utf-8"))

    def log_request(code, size):
        pass


if __name__ == "__main__":
    server = HTTPServer(("0.0.0.0", 8086), PrometheusProxyHandler)
    server.serve_forever()
