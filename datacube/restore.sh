#!/bin/bash
set +x
source .env
set -x

STOP_SERVICES="sfs nsiws-dc-design nsiws-dc-release transfer-service auth-service keycloak mariadb"

docker compose stop ${STOP_SERVICES}

cp /mnt/datacube/keycloakdb.mv.db.bak kc-data/keycloakdb.mv.db || exit 1

docker compose run mariadb bash -c  "rm -r /var/lib/mysql; mariabackup --copy-back --target-dir=/backup/mariadb-latest; chown -R mysql:mysql /var/lib/mysql"

docker exec mongo mongorestore --drop /backup || exit 1

curl "http://localhost:8983/solr/dc_shard1_replica_n1/replication?command=restore&location=/backup&name=latest"

docker compose start ${STOP_SERVICES}
