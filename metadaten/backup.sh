tar --create --xattrs \
    --file=/mnt/metadaten/metadaten.tar --directory=/var/lib \
    --exclude=datasets.old --exclude=datasets.new --xattrs-include='user.*' \
    --warning=no-file-changed metadaten

ec=$?
if [[ $ec != 0 && $ec != 1 ]]
then
  echo "Fatal or unknown error code $ec"
  exit $ec
fi

sync
