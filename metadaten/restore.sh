if systemctl is-enabled --quiet server.service; then systemctl stop server.service; fi

rm --recursive --force /var/lib/metadaten

tar --extract --xattrs \
    --file=/mnt/metadaten/metadaten.tar --directory=/var/lib

if systemctl is-enabled --quiet server.service; then systemctl start server.service; fi
