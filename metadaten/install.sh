export DEBIAN_FRONTEND=noninteractive

apt-get update --yes
apt-get full-upgrade --yes

apt-get install --yes \
    systemd-journal-remote \
    prometheus-node-exporter \
    libjemalloc2 \
    libpoppler-glib8t64 libqpdf29t64

snap install chromium

echo "Cache=no" >>/etc/systemd/resolved.conf

systemctl restart systemd-resolved.service

cat >>/etc/systemd/journald.conf <<EOF
MaxRetentionSec=1week
MaxFileSec=1day
EOF

systemctl restart systemd-journald.service

cat >>/etc/systemd/journal-upload.conf <<EOF
URL=http://status:19532
EOF

mkdir /etc/systemd/system/systemd-journal-upload.service.d
cat >/etc/systemd/system/systemd-journal-upload.service.d/override.conf <<EOF
[Service]
Restart=always

[Unit]
StartLimitIntervalSec=0
EOF

systemctl enable --now systemd-journal-upload.service

echo "LABEL=metadaten /mnt/metadaten ext4 defaults 0 1" >>/etc/fstab
mkdir /mnt/metadaten
mount /mnt/metadaten

useradd --create-home --shell /bin/bash chromedriver
loginctl enable-linger chromedriver

mkdir --parents /home/chromedriver/Downloads
chown chromedriver:chromedriver /home/chromedriver/Downloads
chmod o+x /home/chromedriver
chmod o+rwx /home/chromedriver/Downloads

mkdir --parents /home/chromedriver/.config/systemd/user/default.target.wants
chown chromedriver:chromedriver /home/chromedriver/.config/systemd/user/default.target.wants

cat >/home/chromedriver/.config/systemd/user/chromedriver.service <<EOF
[Unit]
Description=ChromeDriver

[Service]
Type=simple
Restart=always
ExecStart=/snap/bin/chromium.chromedriver --port=9515

[Install]
WantedBy=default.target
EOF

systemctl --user --machine=chromedriver@ daemon-reload
systemctl --user --machine=chromedriver@ start chromedriver.service
systemctl --user --machine=chromedriver@ enable chromedriver.service

systemctl mask harvester.timer
