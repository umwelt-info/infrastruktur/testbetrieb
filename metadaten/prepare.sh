mkdir --parents /etc/systemd/system/harvester.service.d
cat >/etc/systemd/system/harvester.service.d/override.conf <<EOF
[Service]
Environment=RAYON_NUM_THREADS=4
Environment=LD_PRELOAD=libjemalloc.so.2
Environment=BROWSER_DOWNLOAD_DIR=/home/chromedriver/Downloads
Environment=UMTHES_BASE_URL=http://sns:3001/umthes/

ProtectHome=tmpfs
BindPaths=/home/chromedriver
EOF

mkdir --parents /etc/systemd/system/indexer.service.d
cat >/etc/systemd/system/indexer.service.d/override.conf <<EOF
[Service]
Environment=RAYON_NUM_THREADS=4
Environment=LD_PRELOAD=libjemalloc.so.2
EOF

mkdir --parents /etc/systemd/system/server.service.d
cat >/etc/systemd/system/server.service.d/override.conf <<EOF
[Service]
Environment=LD_PRELOAD=libjemalloc.so.2
Environment=UMTHES_BASE_URL=http://sns:3001/umthes/
EOF
