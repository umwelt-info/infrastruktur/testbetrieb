mkdir --parents /etc/systemd/system/usage-stats-api.service.d
cat >/etc/systemd/system/usage-stats-api.service.d/override.conf <<EOF
[Service]
MemoryHigh=200M
MemoryMax=250M
EOF

dpkg --install $1
