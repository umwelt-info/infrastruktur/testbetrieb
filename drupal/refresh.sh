cd /var/www/html

export COMPOSER_ALLOW_SUPERUSER=1
composer install

if [ -n "$DATEN_VERWERFEN" ]
then
    rm -rf web/sites/default/files
    mkdir --parents web/sites/default/files/private

    vendor/bin/drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55)' > hash_salt.txt

    vendor/bin/drush --yes site:install

    # Fix up fresh install to match the exported configuration
    vendor/bin/drush --yes entity:delete shortcut
    vendor/bin/drush --yes config-set system.site uuid 84b29e98-d9ab-4c03-964e-37f4636ab55e
else
    vendor/bin/drush --yes updatedb
fi

vendor/bin/drush --yes cache:rebuild

vendor/bin/drush --yes config:import

chown -R www-data:www-data web/sites/default
