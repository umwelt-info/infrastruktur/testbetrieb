systemctl stop nginx.service

rsync --archive --inplace --delete --stats --human-readable \
    /mnt/drupal/var/www/html/web /var/www/html/

mkdir --parents /var/lib/private/usage-stats-api
cp /mnt/drupal/usage-stats.bit /var/lib/private/usage-stats-api/data.bit

systemctl start nginx.service
