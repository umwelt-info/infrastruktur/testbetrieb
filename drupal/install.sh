export DEBIAN_FRONTEND=noninteractive

apt-get update --yes
apt-get full-upgrade --yes

apt-get install --yes \
    systemd-journal-remote \
    prometheus-node-exporter \
    nginx php-fpm \
    php-opcache php-apcu \
    php-pdo php-sqlite3 \
    php-dom php-xml php-zip php-gd \
    composer \
    zip sqlite3

echo "Cache=no" >>/etc/systemd/resolved.conf

systemctl restart systemd-resolved.service

cat >>/etc/systemd/journald.conf <<EOF
MaxRetentionSec=1week
MaxFileSec=1day
EOF

systemctl restart systemd-journald.service

cat >>/etc/systemd/journal-upload.conf <<EOF
URL=http://status:19532
EOF

mkdir /etc/systemd/system/systemd-journal-upload.service.d
cat >/etc/systemd/system/systemd-journal-upload.service.d/override.conf <<EOF
[Service]
Restart=always

[Unit]
StartLimitIntervalSec=0
EOF

systemctl enable --now systemd-journal-upload.service

cat >/etc/php/8.3/fpm/conf.d/raise-memory-limit.ini <<EOF
memory_limit = 512M
EOF
chmod +x /etc/php/8.3/fpm/conf.d/raise-memory-limit.ini

cat >/etc/php/8.3/fpm/conf.d/raise-upload-limit.ini <<EOF
upload_max_filesize = 250M
post_max_size = 250M
EOF
chmod +x /etc/php/8.3/fpm/conf.d/raise-upload-limit.ini

systemctl reload php8.3-fpm.service

echo "LABEL=drupal /mnt/drupal ext4 defaults 0 1" >>/etc/fstab
mkdir /mnt/drupal
mount /mnt/drupal
