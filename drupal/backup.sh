systemctl stop nginx.service

rsync --archive --inplace --delete --stats --human-readable \
    --relative /var/www/html/web/sites/default/files /mnt/drupal

cp /var/lib/private/usage-stats-api/data.bit /mnt/drupal/usage-stats.bit

systemctl start nginx.service

sync
