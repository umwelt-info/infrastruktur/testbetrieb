export DEBIAN_FRONTEND=noninteractive

apt-get update --yes
apt-get full-upgrade --yes

apt-get install --yes \
    rsync \
    systemd-journal-remote \
    prometheus-node-exporter \
    debian-keyring debian-archive-keyring \
    apt-transport-https \
    postfix

echo "Cache=no" >>/etc/systemd/resolved.conf

systemctl restart systemd-resolved.service

cat >>/etc/systemd/journald.conf <<EOF
MaxRetentionSec=1week
MaxFileSec=1day
EOF

systemctl restart systemd-journald.service

cat >>/etc/systemd/journal-upload.conf <<EOF
URL=http://status:19532
EOF

mkdir /etc/systemd/system/systemd-journal-upload.service.d
cat >/etc/systemd/system/systemd-journal-upload.service.d/override.conf <<EOF
[Service]
Restart=always

[Unit]
StartLimitIntervalSec=0
EOF

systemctl enable --now systemd-journal-upload.service

curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | tee /etc/apt/sources.list.d/caddy-testing.list

apt-get update --yes
apt-get install --yes caddy

echo "LABEL=proxy /mnt/proxy ext4 defaults 0 1" >>/etc/fstab
mkdir /mnt/proxy
mount /mnt/proxy

echo tb.umwelt.info >/etc/mailname

cat >/etc/postfix/main.cf <<"EOF"
biff = no
readme_directory = no
compatibility_level = 3.6
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
smtp_tls_security_level = encrypt
smtpd_relay_restrictions = permit_mynetworks reject_unauth_destination
relay_domains = !sns.uba.de, uba.de, eviden.com
myorigin = /etc/mailname
myhostname = tb.umwelt.info
mydestination = $myhostname, localhost
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 192.168.1.0/24
mailbox_size_limit = 0
recipient_delimiter = +
EOF

systemctl reload postfix.service
