systemctl stop caddy.service

chown -R \
    caddy:caddy \
    /mnt/proxy/var/lib/caddy

rsync --archive --inplace --delete --stats --human-readable \
    /mnt/proxy/var/lib/caddy /var/lib/

systemctl start caddy.service
