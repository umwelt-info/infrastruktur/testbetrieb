systemctl stop caddy.service

rsync --archive --inplace --delete --stats --human-readable \
    --relative /var/lib/caddy /mnt/proxy

systemctl start caddy.service

sync
