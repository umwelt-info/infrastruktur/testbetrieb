export DEBIAN_FRONTEND=noninteractive

apt-get update --yes
apt-get full-upgrade --yes

apt-get install --yes \
    systemd-journal-remote \
    prometheus-node-exporter \
    docker.io docker-compose-v2 \
    postgresql postgresql-contrib

echo "Cache=no" >>/etc/systemd/resolved.conf

systemctl restart systemd-resolved.service

cat >>/etc/systemd/journald.conf <<EOF
MaxRetentionSec=1week
MaxFileSec=1day
EOF

systemctl restart systemd-journald.service

cat >>/etc/systemd/journal-upload.conf <<EOF
URL=http://status:19532
EOF

mkdir /etc/systemd/system/systemd-journal-upload.service.d
cat >/etc/systemd/system/systemd-journal-upload.service.d/override.conf <<EOF
[Service]
Restart=always

[Unit]
StartLimitIntervalSec=0
EOF

systemctl enable --now systemd-journal-upload.service

# avoid docker-related log spam
mkdir /etc/systemd/system/run-docker-.mount.d
cat >/etc/systemd/system/run-docker-.mount.d/silence.conf <<EOF
[Mount]
LogLevelMax=notice
EOF
systemctl daemon-reload

# configure docker to log to journald
cat >/etc/docker/daemon.json <<EOF
{
  "log-driver": "journald",
  "log-level": "warn",
  "log-opts": {
    "tag": "docker/{{.Name}}/{{.ID}}"
  }
}
EOF
systemctl restart docker.service

echo "LABEL=sns /mnt/sns ext4 defaults 0 1" >>/etc/fstab
mkdir /mnt/sns
mount /mnt/sns

cat >/etc/postgresql/16/main/pg_hba.conf <<EOF
local       all     postgres                    peer
hostnossl   all     iqvoc_umt       localhost   trust
hostnossl   all     iqvoc_chronicle localhost   trust
EOF

systemctl restart postgresql.service
