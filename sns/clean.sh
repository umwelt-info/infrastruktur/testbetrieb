systemctl stop postgresql.service

sudo --user=postgres rm -rf /var/lib/postgresql/16/main
sudo --user=postgres /usr/lib/postgresql/16/bin/initdb /var/lib/postgresql/16/main

systemctl start postgresql.service
