export VERSION=v0.49.2
export PORT=$1

curl --output cadvisor --location https://github.com/google/cadvisor/releases/download/${VERSION}/cadvisor-${VERSION}-linux-amd64
chmod +x cadvisor
mv cadvisor /usr/local/bin

cat >/etc/systemd/system/cadvisor.service <<EOF
[Unit]
Description=cAdvisor
After=docker.service containerd.service

[Service]
ExecStart=/usr/local/bin/cadvisor --port=${PORT} --logtostderr=true --docker_only=true

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl restart cadvisor.service
systemctl enable cadvisor.service
