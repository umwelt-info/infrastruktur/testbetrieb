#!/bin/bash
VOLUME_NAME=$1
SIZE=$2
VM_NAME=gitlab-runner

if [ -e /dev/vdb ]; do
    echo "There is already a volume attached."
    exit 1
done

if [ $(hostname) != ${VM_NAME} ]; do
    echo "Must run on ${VM_NAME}"
    exit 1
done

if openstack volume show ${VOLUME_NAME} >/dev/null; then openstack volume delete ${VOLUME_NAME}; fi
openstack volume create --size ${SIZE} ${VOLUME_NAME}
openstack server add volume ${VM_NAME} ${VOLUME_NAME}
until [ -e /dev/vdb ]; do sleep 1s; done
mkfs -t ext4 -L ${VOLUME_NAME} /dev/vdb
openstack server remove volume ${VM_NAME} ${VOLUME_NAME}
