tar cf backup.tar --verbose --directory=/ \
    etc/gitlab-runner/config.toml \
    home/gitlab-runner/.config/openstack/clouds.yaml \
    home/gitlab-runner/.ssh/id_ed25519{,.pub} \
    etc/ssh/ssh_host_{rsa,ecdsa,ed25519}_key{,.pub} \
    home/jump/.ssh/authorized_keys \
    home/jump-metadaten/.ssh/authorized_keys \
    home/jump-sns/.ssh/authorized_keys
