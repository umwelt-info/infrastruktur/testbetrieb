journalctl --sync

sqlite3 \
    /var/lib/private/journal-web-ui/logs.sqlite \
    ".backup /mnt/status/logs.sqlite"

systemctl stop prometheus.service

rsync --archive --inplace --delete --stats --human-readable \
    --relative /var/lib/prometheus /mnt/status

systemctl start prometheus.service

sync
