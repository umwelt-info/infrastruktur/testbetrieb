export DEBIAN_FRONTEND=noninteractive

apt-get update --yes
apt-get full-upgrade --yes

apt-get install --yes \
    rsync \
    systemd-journal-remote \
    libjemalloc2 sqlite3 \
    prometheus prometheus-alertmanager prometheus-node-exporter \

echo "Cache=no" >>/etc/systemd/resolved.conf

systemctl restart systemd-resolved.service

cat >>/etc/systemd/journald.conf <<EOF
MaxRetentionSec=1week
MaxFileSec=1day
EOF

systemctl restart systemd-journald.service

cat >>/etc/systemd/journal-upload.conf <<EOF
URL=http://status:19532
EOF

mkdir /etc/systemd/system/systemd-journal-upload.service.d
cat >/etc/systemd/system/systemd-journal-upload.service.d/override.conf <<EOF
[Service]
Restart=always

[Unit]
StartLimitIntervalSec=0
EOF

systemctl enable --now systemd-journal-upload.service

echo "LABEL=status /mnt/status ext4 defaults 0 1" >>/etc/fstab
mkdir /mnt/status
mount /mnt/status

cat >/etc/default/prometheus <<EOF
ARGS="--storage.tsdb.retention.time=30d"
EOF

systemctl restart prometheus.service
