if systemctl list-unit-files journal-web-ui.service &>/dev/null; then systemctl stop journal-web-ui.service; fi

mkdir -p /var/lib/private/journal-web-ui
rm -f /var/lib/private/journal-web-ui/logs.sqlite*
cp /mnt/status/logs.sqlite /var/lib/private/journal-web-ui/logs.sqlite

if systemctl list-unit-files journal-web-ui.service &>/dev/null; then systemctl start journal-web-ui.service; fi

systemctl stop prometheus.service

chown -R \
    prometheus:prometheus \
    /mnt/status/var/lib/prometheus/alertmanager \
    /mnt/status/var/lib/prometheus/metrics2

rsync --archive --inplace --delete --stats --human-readable \
    /mnt/status/var/lib/prometheus /var/lib/

systemctl start prometheus.service
