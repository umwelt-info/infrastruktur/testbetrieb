mkdir --parents /etc/systemd/system/journal-web-ui.service.d
cat >/etc/systemd/system/journal-web-ui.service.d/override.conf <<EOF
[Service]
Environment=LD_PRELOAD=libjemalloc.so.2
Environment=EXPECTED_MESSAGES=/etc/expected-log-messages
Environment=MAIL_SERVER=proxy MAIL_FROM=journal@tb.umwelt.info MAIL_TO=umwelt.info@uba.de
EOF

dpkg --install $1
