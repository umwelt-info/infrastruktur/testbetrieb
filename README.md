# umwelt.info staging infrastructure automation

This project contains scripts and CI jobs to automate the setup and maintenance of the staging infrastructure supporting the umwelt.info web site.

## Guiding principles

Our approach aims for efficiency, simplicity and automation.

We aim for _efficiency_ and try to apply green computing techniques to minimize the ecological footprint of our service.

To us, this also implies that we provide our service using as little infrastructure as possible. And what we do need, we keep as _simple_ as possible so that we are able to understand it, continuously optimize it and keep it secure.

Since our technical staff is limited, we also aim to _automate_ as much as we reasonable can so that our time can be spent on improving our service instead of maintaining our infrastructure.

## Technical approach

We currently avoid containerization and use one virtual machine per concern. These machines and their attached storage volumes are automatically provisioned using the OpenStack CLI. They run one or more services which may be tightly coupled by sharing the local file system. These services are orchestrated using built-in Linux facilities like systemd units and Debian packages.

For the metadata index, we aim for efficiency of harvesting which is network bound and will stay on a single possibly larger machine. In contrast, serving the index aims for scalability where multiple machines share the same index via a networked file system.

## Structural overview

```mermaid
graph

subgraph "internet"
    user(user)
end

subgraph "testbetrieb"
    proxy[<b>proxy</b><br/>reverse proxy]
    metadaten[<b>metadaten</b><br/>metadata index]
    status[<b>status</b><br/>stores logs<br/>and metrics]
    drupal[<b>drupal</b><br/>Drupal CMS]
    sns[<b>sns</b><br/>Semantic Network Service]
    datacube[<b>datacube</b><br/>Data Cube]

    proxy --> metadaten
    proxy --> drupal
    proxy --> sns
    proxy --> datacube

    proxy <--> status
    metadaten <--> status
    drupal <--> status
    sns <--> status
    datacube <--> status

    user --> proxy
end
```
